webpackJsonp([4],{

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AccessProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ts_md5_dist_md5__ = __webpack_require__(430);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ts_md5_dist_md5___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_ts_md5_dist_md5__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner_ngx__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};





/*
  Generated class for the AccessProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var AccessProvider = /** @class */ (function () {
    /**
    * Creates an instance of AccessProvider.
    */
    function AccessProvider(storage, barcodeScanner, alertController) {
        var _this = this;
        this.storage = storage;
        this.barcodeScanner = barcodeScanner;
        this.alertController = alertController;
        this.isAvailable = false;
        this.pass = "497a6f36735972f55e285859376baae4";
        storage.get('password').then(function (val) {
            if (val != null && val == _this.pass) {
                _this.isAvailable = true;
            }
            else {
                _this.isAvailable = false;
            }
        });
    }
    /**
    * Compares md5-encrypted password strings and changes app status if correct.
    */
    AccessProvider.prototype.login = function (password) {
        var _this = this;
        password = __WEBPACK_IMPORTED_MODULE_2_ts_md5_dist_md5__["Md5"].hashStr(password);
        this.storage.set("password", password);
        this.storage.get("password").then(function (val) {
            if (val != null && val == _this.pass) {
                _this.isAvailable = true;
            }
            else {
                _this.isAvailable = false;
            }
        });
    };
    /**
    * Login by text input
    */
    AccessProvider.prototype.enableByPassword = function () {
        return __awaiter(this, void 0, void 0, function () {
            var alert;
            var _this = this;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            title: "Freischalten",
                            inputs: [
                                {
                                    name: 'password',
                                    type: 'password',
                                    placeholder: 'Passwort'
                                }
                            ],
                            buttons: [
                                {
                                    text: 'Abbrechen',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                },
                                {
                                    text: 'Ok',
                                    handler: function (data) {
                                        _this.login(data.password);
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /**
    * Login by scanning QR-Code
    */
    AccessProvider.prototype.enableByQRCode = function () {
        var _this = this;
        this.barcodeScanner.scan({
            preferFrontCamera: false,
            showFlipCameraButton: false,
            showTorchButton: true,
            torchOn: false,
            prompt: "Freischalten",
            resultDisplayDuration: 0,
        }).then(function (barcodeData) {
            console.log('Barcode data', JSON.stringify(barcodeData));
            _this.login(barcodeData.text);
        }).catch(function (err) {
            console.log('Error', err);
        });
    };
    AccessProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_barcode_scanner_ngx__["a" /* BarcodeScanner */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["a" /* AlertController */]])
    ], AccessProvider);
    return AccessProvider;
}());

//# sourceMappingURL=access.js.map

/***/ }),

/***/ 152:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditNotePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(80);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the EditNotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var EditNotePage = /** @class */ (function () {
    /**
    * Creates an instance of EditNotePage
    */
    function EditNotePage(navCtrl, navParams, storage) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.index = -1;
        this.text = "";
        this.index = this.navParams.get("noteIndex");
        this.storage.get("notes").then(function (data) {
            _this.notes = JSON.parse(data);
            if (_this.index > -1) {
                _this.text = _this.notes[_this.index];
            }
        });
    }
    /**
    * Saves note and returns to preview page
    */
    EditNotePage.prototype.save = function () {
        if (this.index == -1) {
            this.notes.push(this.text);
        }
        else {
            this.notes[this.index] = this.text;
        }
        this.storage.set("notes", JSON.stringify(this.notes));
        this.navCtrl.pop();
    };
    EditNotePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-edit-note',template:/*ion-inline-start:"/Users/henrik/Documents/bewerbung/src/pages/edit-note/edit-note.html"*/'<!--\n  Generated template for the EditNotePage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title *ngIf="index==-1">\n      Neue Notiz\n    </ion-title>\n    <ion-title *ngIf="index>-1">\n      Notiz bearbeiten\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content>\n  <div class="background">\n    <ion-card>\n      <div class="text">\n        <ion-textarea [(ngModel)]="text" placeholder="Notiz"></ion-textarea>\n      </div>\n    </ion-card>\n  </div>\n  <ion-fab *ngIf="!reorder" center bottom>\n    <button [disabled]="text==\'\'" ion-fab (click)="save()"><ion-icon name="checkmark"></ion-icon></button>\n  </ion-fab>\n</ion-content>\n'/*ion-inline-end:"/Users/henrik/Documents/bewerbung/src/pages/edit-note/edit-note.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */]])
    ], EditNotePage);
    return EditNotePage;
}());

//# sourceMappingURL=edit-note.js.map

/***/ }),

/***/ 153:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NotesPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__edit_note_edit_note__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing_ngx__ = __webpack_require__(227);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





/**
 * Generated class for the NotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var NotesPage = /** @class */ (function () {
    /**
    * Creates an instance of NotesPage
    */
    function NotesPage(navCtrl, navParams, storage, socialSharing) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.storage = storage;
        this.socialSharing = socialSharing;
        this.notes = [];
        this.reorder = false;
    }
    /**
    * Opens EditNotePage by index
    */
    NotesPage.prototype.goToEditNote = function (index) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__edit_note_edit_note__["a" /* EditNotePage */], { noteIndex: index });
    };
    /**
    * Refreshes notes
    */
    NotesPage.prototype.ionViewDidEnter = function () {
        var _this = this;
        this.storage.get("notes").then(function (data) {
            if (data != null) {
                _this.notes = JSON.parse(data);
            }
            else {
                _this.storage.set("notes", JSON.stringify([]));
            }
        });
    };
    /**
    * Reorders notes
    */
    NotesPage.prototype.reorderItems = function (indexes) {
        var element = this.notes[indexes.from];
        this.notes.splice(indexes.from, 1);
        this.notes.splice(indexes.to, 0, element);
        this.save();
    };
    /**
    * Saves notes
    */
    NotesPage.prototype.save = function () {
        this.storage.set("notes", JSON.stringify(this.notes));
    };
    /**
    * Show and hide reorder button
    */
    NotesPage.prototype.toggleReorder = function () {
        this.reorder = !this.reorder;
    };
    /**
    * Shares a note by native share client
    */
    NotesPage.prototype.share = function (text) {
        this.socialSharing.share(text);
    };
    /**
    * deletes a note
    */
    NotesPage.prototype.delete = function (noteIndex) {
        this.notes.splice(noteIndex, 1);
        this.save();
    };
    NotesPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-notes',template:/*ion-inline-start:"/Users/henrik/Documents/bewerbung/src/pages/notes/notes.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Notizen</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content no-bounce>\n  <ion-fab center top (click)="goToEditNote(-1)">\n    <button ion-fab><ion-icon name="add"></ion-icon></button>\n  </ion-fab>\n  <ion-fab *ngIf="reorder" right top (click)="toggleReorder()">\n    <button ion-fab><ion-icon name="checkmark"></ion-icon></button>\n  </ion-fab>\n  <ion-fab *ngIf="!reorder" right top (click)="toggleReorder()">\n    <button ion-fab><ion-icon name="list"></ion-icon></button>\n  </ion-fab>\n  <div class="background">\n    <div class="placeholder"></div>\n    <ion-list reorder={{reorder}} (ionItemReorder)="reorderItems($event)">\n      <ion-item-sliding #item *ngFor="let note of notes, let i=index">\n        <ion-item text-wrap>\n          <div class="text" (click)="goToEditNote(i)">\n            <p>\n              {{note}}\n            </p>\n          </div>\n        </ion-item>\n        <ion-item-options side="right">\n          <button class="slideIcon" ion-button (click)="goToEditNote(i)"><ion-icon name="md-create"></ion-icon></button>\n          <button class="slideIcon" ion-button (click)="share(note)"><ion-icon name="share-alt"></ion-icon></button>\n          <button class="slideIcon" ion-button color="danger" (click)="delete(index)"><ion-icon name="trash"></ion-icon></button>\n        </ion-item-options>\n      </ion-item-sliding>\n    </ion-list>\n  </div>\n  <div class="bottom_placeholder"></div>\n</ion-content>\n\n<div class="footer">\n  <div class="triangle1"></div>\n  <div class="triangle2"></div>\n</div>\n'/*ion-inline-end:"/Users/henrik/Documents/bewerbung/src/pages/notes/notes.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_social_sharing_ngx__["a" /* SocialSharing */]])
    ], NotesPage);
    return NotesPage;
}());

//# sourceMappingURL=notes.js.map

/***/ }),

/***/ 154:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StartPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__tabs_tabs__ = __webpack_require__(229);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Generated class for the StartPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var StartPage = /** @class */ (function () {
    /**
    * Creates an instance of StartPage
    */
    function StartPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    /**
    * Go forward to Main Page when animation ended.
    */
    StartPage.prototype.ionViewDidLoad = function () {
        var that = this;
        setTimeout(function () {
            that.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__tabs_tabs__["a" /* TabsPage */]);
        }, 4200);
    };
    StartPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-start',template:/*ion-inline-start:"/Users/henrik/Documents/bewerbung/src/pages/start/start.html"*/'<ion-header>\n</ion-header>\n\n<ion-content no-bounce>\n  <div class="backgroundImage"></div>\n  <div class="birdbox">\n    <div class="bird bird1"></div>\n    <div class="bird bird2"></div>\n  </div>\n</ion-content>\n'/*ion-inline-end:"/Users/henrik/Documents/bewerbung/src/pages/start/start.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], StartPage);
    return StartPage;
}());

//# sourceMappingURL=start.js.map

/***/ }),

/***/ 183:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 183;

/***/ }),

/***/ 225:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/edit-note/edit-note.module": [
		449,
		3
	],
	"../pages/imprint/imprint.module": [
		450,
		2
	],
	"../pages/notes/notes.module": [
		451,
		1
	],
	"../pages/start/start.module": [
		452,
		0
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 225;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 229:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__about_about__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact_contact__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__notes_notes__ = __webpack_require__(153);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = /** @class */ (function () {
    /**
    * Creates instance of TabsPage
    */
    function TabsPage() {
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__about_about__["a" /* AboutPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__contact_contact__["a" /* ContactPage */];
        this.tab4Root = __WEBPACK_IMPORTED_MODULE_4__notes_notes__["a" /* NotesPage */];
    }
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/henrik/Documents/bewerbung/src/pages/tabs/tabs.html"*/'<ion-tabs>\n  <ion-tab [root]="tab1Root" tabTitle="Bewerbung" tabIcon="paper"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="Lebenslauf" tabIcon="person"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="Anhänge" tabIcon="attach"></ion-tab>\n  <!--\n  <ion-tab [root]="tab4Root" tabTitle="Notizen" tabIcon="list"></ion-tab>\n  -->\n\n</ion-tabs>\n'/*ion-inline-end:"/Users/henrik/Documents/bewerbung/src/pages/tabs/tabs.html"*/
        }),
        __metadata("design:paramtypes", [])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 230:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AboutPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser_ngx__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_access_access__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number_ngx__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_email_composer_ngx__ = __webpack_require__(83);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AboutPage = /** @class */ (function () {
    /**
    * Creates an instance of AboutPage
    */
    function AboutPage(navCtrl, iab, alertController, accessProvider, callNumber, emailComposer) {
        this.navCtrl = navCtrl;
        this.iab = iab;
        this.alertController = alertController;
        this.accessProvider = accessProvider;
        this.callNumber = callNumber;
        this.emailComposer = emailComposer;
    }
    /**
    * Opens xing in system browser
    */
    AboutPage.prototype.goToXing = function () {
        var url = "https://www.xing.com/profile/Henrik_Meyer16";
        var options = "location=no,closebuttoncaption=Fertig,hardwareback=yes,toolbarposition=top,closebuttoncolor=#FFF100,zoom=yes,toolbarcolor=#707070,enableViewportScale=yes,presentationstyle=formsheet,hideurlbar=yes";
        var browser = this.iab.create(url, '_system', options);
    };
    /**
    * Opens linkedin in system browser
    */
    AboutPage.prototype.goToLinkedIn = function () {
        var url = "https://www.linkedin.com/in/henrik-meyer-456266109";
        var options = "location=no,closebuttoncaption=Fertig,hardwareback=yes,toolbarposition=top,closebuttoncolor=#FFF100,zoom=yes,toolbarcolor=#707070,enableViewportScale=yes,presentationstyle=formsheet,hideurlbar=yes";
        var browser = this.iab.create(url, '_system', options);
    };
    /**
    * Opens website in sytem browser
    */
    AboutPage.prototype.goToWebsite = function () {
        var url = "https://www.henry-spaceman.com";
        var options = "location=no,closebuttoncaption=Fertig,hardwareback=yes,toolbarposition=top,closebuttoncolor=#FFF100,zoom=yes,toolbarcolor=#707070,enableViewportScale=yes,presentationstyle=formsheet,hideurlbar=yes";
        var browser = this.iab.create(url, '_system', options);
    };
    /**
    * Opens the native Call-Client with number
    */
    AboutPage.prototype.call = function () {
        this.callNumber.callNumber("017684350106", true)
            .then(function (res) {
            console.log('Launched dialer!', res);
        })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    /**
    * Opens the native Mail-Client with recipient and subject
    */
    AboutPage.prototype.mail = function () {
        var email = {
            to: 'henrik.mey@web.de',
            subject: 'Bewerbung Henrik Meyer',
            body: ''
        };
        this.emailComposer.open(email);
    };
    AboutPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"/Users/henrik/Documents/bewerbung/src/pages/about/about.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Lebenslauf\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content no-bounce>\n  <div *ngIf="!accessProvider.isAvailable" class="lockContainer">\n    <ion-card>\n      <div class="lock">\n        <ion-icon name="lock"></ion-icon>\n      </div>\n      <div class="text">\n        <p>\n          Dieser Bereich ist Passwortgeschützt. Ein entsprechendes Passwort liegt den Ihnen zugeschickten Unterlagen bei, oder kann per E-Mail angefordert werden:\n          <a href="mailto:info@henry-spaceman.com">Passwort anfordern</a>\n        </p>\n      </div>\n    </ion-card>\n    <button full ion-button (click)="accessProvider.enableByPassword()"><ion-icon name="create" class="icon_placeholder"></ion-icon>Freischalten mit Passwort</button>\n    <button full ion-button (click)="accessProvider.enableByQRCode()"><ion-icon name="qr-scanner" class="icon_placeholder"></ion-icon>Freischalten mit QR-Code</button>\n  </div>\n  <div *ngIf="accessProvider.isAvailable">\n    <div class="profileimage fadeIn"></div>\n    <ion-list no-lines>\n      <ion-item-divider>\n        <ion-icon item-left name="briefcase" color="primary"></ion-icon>Berufserfahrung\n      </ion-item-divider>\n      <ion-item text-wrap>\n        <div item-left>\n          2016 – 2019\n        </div>\n        Web- & App-Entwickler<br>\n        Quantumfrog GmbH\n      </ion-item>\n      <ion-item text-wrap>\n        <div item-left>\n          2015 – 2016\n        </div>\n        Praxisphase mit Bachelorarbeit<br>\n        Quantumfrog GmbH\n      </ion-item>\n      <ion-item-divider>\n        <ion-icon item-left name="school" color="primary"></ion-icon> Ausbildung\n      </ion-item-divider>\n      <ion-item text-wrap>\n        <div item-left>\n          2011 – 2016\n        </div>\n        Studium Medientechnik<br>\n        (Bachelor of Engineering)<br>\n        Hochschule Emden/Leer\n      </ion-item>\n      <ion-item text-wrap>\n        <div item-left>\n          2003 – 2010\n        </div>\n        Neues Gymnasium Oldenburg (Abitur)\n      </ion-item>\n      <ion-item text-wrap>\n        <div item-left>\n          2001 – 2003\n        </div>\n        Orientierungsstufe Flötenteich\n      </ion-item>\n      <ion-item text-wrap>\n        <div item-left>\n          1997 – 2001\n        </div>\n        Grundschule Etzhorn\n      </ion-item>\n      <ion-item-divider>\n        <ion-icon item-left name="bulb" color="primary"></ion-icon>Fachspezifische Fähigkeiten\n      </ion-item-divider>\n      <ion-list-header>\n        Frameworks:\n      </ion-list-header>\n      <ion-item text-wrap>\n        Cordova, <ion-icon name="ios-ionic" class="texticon"></ion-icon>Ionic, <ion-icon name="logo-angular" class="texticon"></ion-icon>Angular, <ion-icon name="logo-wordpress" class="texticon"></ion-icon>Wordpress\n      </ion-item>\n      <ion-list-header>\n        Programmier- und Auszeichnungssprachen:\n      </ion-list-header>\n      <ion-item text-wrap>\n        Objective-C, Swift, JavaScript, TypeScript, HTML(5), (S)CSS, PHP, SQL\n      </ion-item>\n      <ion-list-header>\n        Entwicklungsumgebungen:\n      </ion-list-header>\n      <ion-item text-wrap>\n        Xcode, Eclipse\n      </ion-item>\n      <ion-list-header>\n        Weitere Erfahrungen:\n      </ion-list-header>\n      <ion-item text-wrap>\n        Microsoft Azure, Microsoft Office, Adobe Photoshop, Adobe Illustrator, Apache Server\n      </ion-item>\n      <ion-item-divider>\n          <ion-icon item-left name="ice-cream" color="primary"></ion-icon>Freizeitaktivitäten\n      </ion-item-divider>\n      <ion-item text-wrap>\n        Umsetzung eigener Software-Ideen\n      </ion-item>\n      <ion-item text-wrap>\n        Experimentelle Klangerzeugung auf Klavier- und E-Piono, Gitarre\n      </ion-item>\n      <ion-item text-wrap>\n        Freies Lauf- und Körpertraining\n      </ion-item>\n      <ion-item text-wrap>\n        Bogenschießen\n      </ion-item>\n      <ion-item-divider>\n          <ion-icon item-left name="contact" color="primary"></ion-icon>Kontakt\n      </ion-item-divider>\n      <ion-item (click)="call()">\n        <ion-icon name="call" item-left></ion-icon>\n        017684350106\n      </ion-item>\n      <ion-item (click)="mail()">\n        <ion-icon name="mail" item-left></ion-icon>\n        henrik.mey@web.de\n      </ion-item>\n    </ion-list>\n  </div>\n  <div class="bottom_placeholder"></div>\n  <ion-fab right top *ngIf="accessProvider.isAvailable">\n    <button ion-fab><ion-icon name="add"></ion-icon></button>\n    <ion-fab-list side="bottom">\n      <button ion-fab class="xing" (click)="goToXing()"></button>\n      <button ion-fab class="linkedin" (click)="goToLinkedIn()"></button>\n      <button ion-fab (click)="call()"><ion-icon name="call"></ion-icon></button>\n      <button ion-fab (click)="mail()"><ion-icon name="mail"></ion-icon></button>\n      <!--<button ion-fab (click)="goToWebsite()"><ion-icon name="at"></ion-icon></button>-->\n    </ion-fab-list>\n  </ion-fab>\n</ion-content>\n<div class="footer">\n  <div class="triangle1"></div>\n  <div class="triangle2"></div>\n</div>\n'/*ion-inline-end:"/Users/henrik/Documents/bewerbung/src/pages/about/about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_access_access__["a" /* AccessProvider */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_call_number_ngx__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_email_composer_ngx__["a" /* EmailComposer */]])
    ], AboutPage);
    return AboutPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 232:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ContactPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_document_viewer_ngx__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_opener_ngx__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_ngx__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_path_ngx__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_access_access__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_call_number_ngx__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_email_composer_ngx__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser_ngx__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};










var ContactPage = /** @class */ (function () {
    /**
    * Creates an instance of ContactPage
    */
    function ContactPage(navCtrl, documentViewer, file, fileOpener, alertController, filePath, platform, accessProvider, callNumber, emailComposer, iab) {
        this.navCtrl = navCtrl;
        this.documentViewer = documentViewer;
        this.file = file;
        this.fileOpener = fileOpener;
        this.alertController = alertController;
        this.filePath = filePath;
        this.platform = platform;
        this.accessProvider = accessProvider;
        this.callNumber = callNumber;
        this.emailComposer = emailComposer;
        this.iab = iab;
    }
    /**
    * Opens PDF in native app
    */
    ContactPage.prototype.showBewerbung = function () {
        var _this = this;
        var path = this.file.applicationDirectory + 'www/assets';
        var fileName = "Bewerbung_HenrikMeyer.pdf";
        if (!this.platform.is("ios")) {
            path = this.file.applicationDirectory;
            this.filePath.resolveNativePath(path).then(function (native_path) {
                var options = {
                    title: 'Bewerbung Henrik Meyer'
                };
                _this.file.copyFile(native_path + "www/assets", fileName, _this.file.externalDataDirectory, fileName).then(function (copied) {
                    console.log("FILESYSTEM2: copied:" + JSON.stringify(copied));
                    _this.fileOpener.open(_this.file.externalDataDirectory + fileName, 'application/pdf')
                        .then(function () { return console.log("FILESYSTEM3: FILE OPENED"); })
                        .catch(function (e) { return console.log('FILESYSTEM3: Error opening file' + JSON.stringify(e)); });
                }).catch(function (err) {
                    console.log("FILESYSTEM2: not copied " + JSON.stringify(err));
                });
            });
        }
        else {
            var fileName_1 = "Bewerbung_HenrikMeyer.pdf";
            var options = {
                title: 'Bewerbung Henrik Meyer'
            };
            this.fileOpener.open(path + "/" + fileName_1, 'application/pdf')
                .then(function () { return console.log('File is opened'); })
                .catch(function (e) { return console.log('Error opening file', e); });
        }
    };
    /**
    * Opens PDF in native app
    */
    ContactPage.prototype.showArbeitszeugnis = function () {
        var _this = this;
        var path = this.file.applicationDirectory + 'www/assets';
        var fileName = "Arbeitszeugnis.pdf";
        if (!this.platform.is("ios")) {
            path = this.file.applicationDirectory;
            this.filePath.resolveNativePath(path).then(function (native_path) {
                var options = {
                    title: 'Arbeitszeugnis'
                };
                _this.file.copyFile(native_path + "www/assets", fileName, _this.file.externalDataDirectory, fileName).then(function (copied) {
                    console.log("FILESYSTEM2: copied:" + JSON.stringify(copied));
                    _this.fileOpener.open(_this.file.externalDataDirectory + fileName, 'application/pdf')
                        .then(function () { return console.log("FILESYSTEM3: FILE OPENED"); })
                        .catch(function (e) { return console.log('FILESYSTEM3: Error opening file' + JSON.stringify(e)); });
                }).catch(function (err) {
                    console.log("FILESYSTEM2: not copied " + JSON.stringify(err));
                });
            });
        }
        else {
            var fileName_2 = "Arbeitszeugnis.pdf";
            var options = {
                title: 'Arbeitszeugnis'
            };
            this.fileOpener.open(path + "/" + fileName_2, 'application/pdf')
                .then(function () { return console.log('File is opened'); })
                .catch(function (e) { return console.log('Error opening file', e); });
        }
    };
    /**
    * Opens PDF in native app
    */
    ContactPage.prototype.showBachelorurkunde = function () {
        var _this = this;
        var path = this.file.applicationDirectory + 'www/assets';
        var fileName = "Bachelorurkunde.pdf";
        if (!this.platform.is("ios")) {
            path = this.file.applicationDirectory;
            this.filePath.resolveNativePath(path).then(function (native_path) {
                var options = {
                    title: 'Bachelorurkunde'
                };
                _this.file.copyFile(native_path + "www/assets", fileName, _this.file.externalDataDirectory, fileName).then(function (copied) {
                    console.log("FILESYSTEM2: copied:" + JSON.stringify(copied));
                    _this.fileOpener.open(_this.file.externalDataDirectory + fileName, 'application/pdf')
                        .then(function () { return console.log("FILESYSTEM3: FILE OPENED"); })
                        .catch(function (e) { return console.log('FILESYSTEM3: Error opening file' + JSON.stringify(e)); });
                }).catch(function (err) {
                    console.log("FILESYSTEM2: not copied " + JSON.stringify(err));
                });
            });
        }
        else {
            var fileName_3 = "Bachelorurkunde.pdf";
            var options = {
                title: 'Bachelorurkunde'
            };
            this.fileOpener.open(path + "/" + fileName_3, 'application/pdf')
                .then(function () { return console.log('File is opened'); })
                .catch(function (e) { return console.log('Error opening file', e); });
        }
    };
    /**
    * Opens PDF in native app
    */
    ContactPage.prototype.showBetriebshelfer = function () {
        var _this = this;
        var path = this.file.applicationDirectory + 'www/assets';
        var fileName = "Betriebshelfer.pdf";
        if (!this.platform.is("ios")) {
            path = this.file.applicationDirectory;
            this.filePath.resolveNativePath(path).then(function (native_path) {
                var options = {
                    title: 'Betriebshelfer-Bescheinigung'
                };
                _this.file.copyFile(native_path + "www/assets", fileName, _this.file.externalDataDirectory, fileName).then(function (copied) {
                    console.log("FILESYSTEM2: copied:" + JSON.stringify(copied));
                    _this.fileOpener.open(_this.file.externalDataDirectory + fileName, 'application/pdf')
                        .then(function () { return console.log("FILESYSTEM3: FILE OPENED"); })
                        .catch(function (e) { return console.log('FILESYSTEM3: Error opening file' + JSON.stringify(e)); });
                }).catch(function (err) {
                    console.log("FILESYSTEM2: not copied " + JSON.stringify(err));
                });
            });
        }
        else {
            var fileName_4 = "Betriebshelfer.pdf";
            var options = {
                title: 'Betriebshelfer-Bescheinigung'
            };
            this.fileOpener.open(path + "/" + fileName_4, 'application/pdf')
                .then(function () { return console.log('File is opened'); })
                .catch(function (e) { return console.log('Error opening file', e); });
        }
    };
    /**
    * Opens AGBs in system browser
    */
    ContactPage.prototype.showDatenschutz = function () {
        var options = "";
        var browser = this.iab.create('https://www.henry-spaceman.com/datenschutz', '_system', options);
    };
    /**
    * Opens native phone app with number
    */
    ContactPage.prototype.call = function () {
        this.callNumber.callNumber("017684350106", true)
            .then(function (res) {
            console.log('Launched dialer!', res);
        })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    ContactPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"/Users/henrik/Documents/bewerbung/src/pages/contact/contact.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>\n      Anhänge\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n<ion-content no-bounce>\n  <div *ngIf="!accessProvider.isAvailable" class="lockContainer">\n    <ion-card>\n      <div class="lock">\n        <ion-icon name="lock"></ion-icon>\n      </div>\n      <div class="text">\n        <p>\n          Dieser Bereich ist Passwortgeschützt. Ein entsprechendes Passwort liegt den Ihnen zugeschickten Unterlagen bei, oder kann per E-Mail angefordert werden:\n          <a href="mailto:info@henry-spaceman.com">Passwort anfordern</a>\n        </p>\n      </div>\n    </ion-card>\n    <button full ion-button (click)="accessProvider.enableByPassword()"><ion-icon name="create"></ion-icon>Freischalten mit Passwort</button>\n    <button full ion-button (click)="accessProvider.enableByQRCode()"><ion-icon name="qr-scanner"></ion-icon>Freischalten mit QR-Code</button>\n  </div>\n  <div *ngIf="accessProvider.isAvailable">\n    <ion-list no-lines>\n      <ion-item (click)="showArbeitszeugnis()">\n        <ion-icon name="paper" item-start></ion-icon>\n        Letztes Arbeitszeugnis\n      </ion-item>\n      <ion-item (click)="showBachelorurkunde()">\n        <ion-icon name="paper" item-start></ion-icon>\n        Bachelorurkunde\n      </ion-item>\n      <ion-item (click)="showBewerbung()">\n        <ion-icon name="paper" item-start></ion-icon>\n        Bewerbung als PDF\n      </ion-item>\n      <ion-item (click)="showBetriebshelfer()">\n        <ion-icon name="paper" item-start></ion-icon>\n        Betriebshelfer-Bescheinigung\n      </ion-item>\n    </ion-list>\n  </div>\n  <div class="bottom_placeholder"></div>\n</ion-content>\n<div class="footer">\n  <div class="triangle1"></div>\n  <div class="triangle2"></div>\n</div>\n'/*ion-inline-end:"/Users/henrik/Documents/bewerbung/src/pages/contact/contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_document_viewer_ngx__["a" /* DocumentViewer */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_file_ngx__["a" /* File */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_file_opener_ngx__["a" /* FileOpener */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__ionic_native_file_path_ngx__["a" /* FilePath */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_6__providers_access_access__["a" /* AccessProvider */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_call_number_ngx__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_email_composer_ngx__["a" /* EmailComposer */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */]])
    ], ContactPage);
    return ContactPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 237:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number_ngx__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_email_composer_ngx__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser_ngx__ = __webpack_require__(81);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var HomePage = /** @class */ (function () {
    /**
    * Creates an instance of HomePage
    */
    function HomePage(navCtrl, emailComposer, callNumber, iab) {
        this.navCtrl = navCtrl;
        this.emailComposer = emailComposer;
        this.callNumber = callNumber;
        this.iab = iab;
    }
    /**
    * Opens native phone app
    */
    HomePage.prototype.call = function () {
        this.callNumber.callNumber("017684350106", true)
            .then(function (res) {
            console.log('Launched dialer!', res);
        })
            .catch(function (err) { return console.log('Error launching dialer', err); });
    };
    /**
    * Opens native mail client
    */
    HomePage.prototype.mail = function () {
        var email = {
            to: 'henrik.mey@web.de',
            subject: 'Bewerbung Henrik Meyer',
            body: ''
        };
        // Send a text message using default options
        this.emailComposer.open(email);
    };
    /**
    * Opens AGBs in system browser
    */
    HomePage.prototype.goToImprint = function () {
        var url = "https://henry-spaceman.com/impressum";
        var options = "location=no,closebuttoncaption=Fertig,hardwareback=yes,toolbarposition=top,closebuttoncolor=#FFF100,zoom=yes,toolbarcolor=#707070,enableViewportScale=yes,presentationstyle=formsheet,hideurlbar=yes";
        var browser = this.iab.create(url, '_system', options);
    };
    /**
    * Opens AGBs in system browser
    */
    HomePage.prototype.goToTerms = function () {
        var url = "https://henry-spaceman.com/datenschutz";
        var options = "location=no,closebuttoncaption=Fertig,hardwareback=yes,toolbarposition=top,closebuttoncolor=#FFF100,zoom=yes,toolbarcolor=#707070,enableViewportScale=yes,presentationstyle=formsheet,hideurlbar=yes";
        var browser = this.iab.create(url, '_system', options);
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"/Users/henrik/Documents/bewerbung/src/pages/home/home.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Bewerbung</ion-title>\n    <!--\n    <ion-buttons end>\n      <button ion-button (click)="goToImprint()">Impressum</button>\n    </ion-buttons>\n    -->\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content no-bounce>\n  <!--\n  <div class="logo_container">\n    <div class="logo heinekingmedia1"></div>\n    <div class="logo heinekingmedia2"></div>\n    <div class="logo heinekingmedia3"></div>\n    <div class="logo heinekingmedia4"></div>\n  </div>\n  -->\n  <div class="birdbox">\n    <div class="bird bird1"></div>\n    <div class="bird bird2"></div>\n  </div>\n  <div class="background">\n    <ion-card>\n      <div class="text">\n        <p>\n          Guten Tag,\n        </p>\n        <br>\n        <p>\n          vielen Dank, dass Sie sich meine Bewerbungs-App heruntergeladen haben. Da diese App öffentlich zugänglich ist, geht sie nicht direkt auf Ihr Unternehmen ein und unterscheidet sich deshalb entsprechend von der Papierform. Aus dem gleichen Grund sind auch mein Lebenslauf und die Anhänge passwortgeschützt. Ein entsprechendes Passwort liegt der Papierform bei, oder kann per E-Mail angefordert werden:\n          <a href="mailto:info@henry-spaceman.com">Passwort anfordern</a>\n        </p>\n        <br>\n        <p>\n          Ich bewerbe ich mich als App-Entwickler in Ihrem Hause.\n        </p>\n        <p>\n          Ich bin 28 Jahre alt und in Oldenburg aufgewachsen. Nach meinem Zivildienst habe ich Medientechnik an der Hochschule in Emden studiert, meinen Bachelor-Abschluss erlangt und arbeite seitdem für die Quantumfrog GmbH aus Oldenburg. Vor kurzem bin ich nach Hannover gezogen und suche hier vor Ort nach neuen Herausforderungen.<br>\n        </p>\n        <p>\n          Bereits während meines Studiums der Medientechnik an der Hochschule in Emden durfte ich feststellen, welche technischen und kreativen Möglichkeiten die Web- und Appentwicklung bietet. Bei meinem jetzigen Arbeitgeber habe ich mich auf die Appentwicklung spezialisiert. Dabei bin ich sowohl vertraut mit der nativen iOS-Entwicklung (Objective-C & Swift) als auch mit der webbasierten Crossplattform-Entwicklung (Angular JS, Cordova, Ionic) für iOS/Android/Mac/PC und habe bereits viele Projekte für verschiedene große Kunden erfolgreich umgesetzt. Dazu gehören eine der größten deutschen gesetzlichen Krankenversicherungen sowie ein hiesiges Energieunternehmen und zahlreiche Privatunternehmer.\n        </p>\n        <p>\n          Ich bin offen für verschiedene Arbeitsweisen und habe Freude daran, mich in neue Aufgabengebiete und Programmiersprachen einzuarbeiten.\n        </p>\n        <p>\n          Gerne komme ich für ein Gespräch vorbei oder stehe für ein Telefonat bereit, um weitere Fragen zu klären.\n        </p>\n        <br>\n        <p>\n          Liebe Grüße\n        </p>\n        <div class="sign"></div>\n        <p>\n          Henrik Meyer<br>\n          Softwareentwickler\n        </p>\n      </div>\n    </ion-card>\n    <br>\n    <div class="link" (click)="goToImprint()">Impressum</div>\n    <br>\n    <div class="link" (click)="goToTerms()">Datenschutz</div>\n  </div>\n  <div class="bottom_placeholder"></div>\n</ion-content>\n\n<div class="footer">\n  <div class="triangle1"></div>\n  <div class="triangle2"></div>\n</div>\n'/*ion-inline-end:"/Users/henrik/Documents/bewerbung/src/pages/home/home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_email_composer_ngx__["a" /* EmailComposer */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_call_number_ngx__["a" /* CallNumber */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 280:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ImprintPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Generated class for the ImprintPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */
var ImprintPage = /** @class */ (function () {
    function ImprintPage(navCtrl, navParams) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
    }
    ImprintPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad ImprintPage');
    };
    ImprintPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-imprint',template:/*ion-inline-start:"/Users/henrik/Documents/bewerbung/src/pages/imprint/imprint.html"*/'<!--\n  Generated template for the ImprintPage page.\n\n  See http://ionicframework.com/docs/components/#navigation for more info on\n  Ionic pages and navigation.\n-->\n<ion-header>\n  <ion-navbar>\n    <ion-title>imprint</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n'/*ion-inline-end:"/Users/henrik/Documents/bewerbung/src/pages/imprint/imprint.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* NavParams */]])
    ], ImprintPage);
    return ImprintPage;
}());

//# sourceMappingURL=imprint.js.map

/***/ }),

/***/ 281:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(282);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(399);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 399:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(46);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(448);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_about_about__ = __webpack_require__(230);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__ = __webpack_require__(232);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_home_home__ = __webpack_require__(237);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__ = __webpack_require__(229);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_start_start__ = __webpack_require__(154);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_notes_notes__ = __webpack_require__(153);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_edit_note_edit_note__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_imprint_imprint__ = __webpack_require__(280);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar_ngx__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen_ngx__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__ionic_native_in_app_browser_ngx__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__ionic_native_barcode_scanner_ngx__ = __webpack_require__(231);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__ionic_native_document_viewer_ngx__ = __webpack_require__(233);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_file_ngx__ = __webpack_require__(235);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_opener_ngx__ = __webpack_require__(234);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_file_path_ngx__ = __webpack_require__(236);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_screen_orientation_ngx__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_storage__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_call_number_ngx__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__ionic_native_email_composer_ngx__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__ionic_native_social_sharing_ngx__ = __webpack_require__(227);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__providers_access_access__ = __webpack_require__(130);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_start_start__["a" /* StartPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_notes_notes__["a" /* NotesPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_edit_note_edit_note__["a" /* EditNotePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_imprint_imprint__["a" /* ImprintPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                    backButtonText: ''
                }, {
                    links: [
                        { loadChildren: '../pages/edit-note/edit-note.module#EditNotePageModule', name: 'EditNotePage', segment: 'edit-note', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/imprint/imprint.module#ImprintPageModule', name: 'ImprintPage', segment: 'imprint', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/notes/notes.module#NotesPageModule', name: 'NotesPage', segment: 'notes', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/start/start.module#StartPageModule', name: 'StartPage', segment: 'start', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_21__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_about_about__["a" /* AboutPage */],
                __WEBPACK_IMPORTED_MODULE_5__pages_contact_contact__["a" /* ContactPage */],
                __WEBPACK_IMPORTED_MODULE_6__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_8__pages_start_start__["a" /* StartPage */],
                __WEBPACK_IMPORTED_MODULE_9__pages_notes_notes__["a" /* NotesPage */],
                __WEBPACK_IMPORTED_MODULE_10__pages_edit_note_edit_note__["a" /* EditNotePage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_imprint_imprint__["a" /* ImprintPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_12__ionic_native_status_bar_ngx__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_13__ionic_native_splash_screen_ngx__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_14__ionic_native_in_app_browser_ngx__["a" /* InAppBrowser */],
                __WEBPACK_IMPORTED_MODULE_15__ionic_native_barcode_scanner_ngx__["a" /* BarcodeScanner */],
                __WEBPACK_IMPORTED_MODULE_16__ionic_native_document_viewer_ngx__["a" /* DocumentViewer */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_file_opener_ngx__["a" /* FileOpener */],
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_file_ngx__["a" /* File */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_file_path_ngx__["a" /* FilePath */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_screen_orientation_ngx__["a" /* ScreenOrientation */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_25__providers_access_access__["a" /* AccessProvider */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_call_number_ngx__["a" /* CallNumber */],
                __WEBPACK_IMPORTED_MODULE_23__ionic_native_email_composer_ngx__["a" /* EmailComposer */],
                __WEBPACK_IMPORTED_MODULE_24__ionic_native_social_sharing_ngx__["a" /* SocialSharing */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 448:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar_ngx__ = __webpack_require__(277);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen_ngx__ = __webpack_require__(278);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_screen_orientation_ngx__ = __webpack_require__(279);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_start_start__ = __webpack_require__(154);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, screenOrientation, alertController, app) {
        var _this = this;
        this.screenOrientation = screenOrientation;
        this.alertController = alertController;
        this.app = app;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_5__pages_start_start__["a" /* StartPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            if (platform.is("cordova")) {
                if (platform.is("ios")) {
                    statusBar.styleDefault();
                }
                else {
                    statusBar.styleLightContent();
                }
                splashScreen.hide();
                screenOrientation.lock(_this.screenOrientation.ORIENTATIONS.PORTRAIT);
            }
            platform.registerBackButtonAction(function () {
                // Catches the active view
                var nav = _this.app.getActiveNavs()[0];
                var activeView = nav.getActive();
                // Checks if can go back before show up the alert
                if (activeView.name === 'TabPage') {
                    if (nav.canGoBack()) {
                        nav.pop();
                    }
                    else {
                        var alert_1 = _this.alertController.create({
                            title: 'App schließen',
                            message: 'Wollen Sie die App schließen?',
                            buttons: [{
                                    text: 'Nein',
                                    role: 'cancel',
                                }, {
                                    text: 'Ja',
                                    handler: function () {
                                        platform.exitApp();
                                    }
                                }]
                        });
                        alert_1.present();
                    }
                }
            });
        });
    }
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"/Users/henrik/Documents/bewerbung/src/app/app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"/Users/henrik/Documents/bewerbung/src/app/app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar_ngx__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen_ngx__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_4__ionic_native_screen_orientation_ngx__["a" /* ScreenOrientation */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ })

},[281]);
//# sourceMappingURL=main.js.map