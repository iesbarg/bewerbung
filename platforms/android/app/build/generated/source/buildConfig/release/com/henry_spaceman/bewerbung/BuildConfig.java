/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.henry_spaceman.bewerbung;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.henry_spaceman.bewerbung";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 19;
  public static final String VERSION_NAME = "1.0.0";
}
