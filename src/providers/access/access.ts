import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Md5 } from 'ts-md5/dist/md5';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertController } from 'ionic-angular';
/*
  Generated class for the AccessProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AccessProvider {

  isAvailable:boolean = false;
  pass:string = "497a6f36735972f55e285859376baae4";

  /**
  * Creates an instance of AccessProvider.
  */
  constructor(
    private storage: Storage,
    private barcodeScanner: BarcodeScanner,
    public alertController: AlertController
  ) {
    storage.get('password').then((val) => {
      if(val!=null&&val==this.pass){
        this.isAvailable = true;
      }
      else{
        this.isAvailable = false;
      }
    });
  }

  /**
  * Compares md5-encrypted password strings and changes app status if correct.
  */
  login(password){
    password = Md5.hashStr(password);
    this.storage.set("password", password);
    this.storage.get("password").then((val) => {
      if(val!=null&&val==this.pass){
        this.isAvailable = true;
      }
      else{
        this.isAvailable = false;
      }
    });
  }

  /**
  * Login by text input
  */
  async enableByPassword() {
    const alert = await this.alertController.create({
      title: "Freischalten",
      inputs: [
        {
          name: 'password',
          type: 'password',
          placeholder: 'Passwort'
        }
      ],
      buttons: [
        {
          text: 'Abbrechen',
          role: 'cancel',
          cssClass: 'secondary',

        },
        {
          text: 'Ok',
          handler: data => {
            this.login(data.password);
          }
        }
      ]
    });
    await alert.present();
  }

  /**
  * Login by scanning QR-Code
  */
  enableByQRCode(){
    this.barcodeScanner.scan(
      {
          preferFrontCamera : false, // iOS and Android
          showFlipCameraButton : false, // iOS and Android
          showTorchButton : true, // iOS and Android
          torchOn: false, // Android, launch with the torch switched on (if available)
          prompt : "Freischalten", // Android
          resultDisplayDuration: 0, // Android, display scanned text for X ms. 0 suppresses it entirely, default 1500
      }
    ).then(barcodeData => {
     console.log('Barcode data', JSON.stringify(barcodeData));
     this.login(barcodeData.text);
    }).catch(err => {
        console.log('Error', err);
    });
  }
}
