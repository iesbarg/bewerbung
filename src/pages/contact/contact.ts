import { Component } from '@angular/core';
import { NavController, AlertController, Platform } from 'ionic-angular';
import { DocumentViewer, DocumentViewerOptions } from '@ionic-native/document-viewer/ngx';
import { FileOpener } from '@ionic-native/file-opener/ngx';
import { File } from '@ionic-native/file/ngx';

import { FilePath } from '@ionic-native/file-path/ngx';
import { AccessProvider } from '../../providers/access/access';

import { CallNumber } from '@ionic-native/call-number/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';


@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})
export class ContactPage {

  /**
  * Creates an instance of ContactPage
  */
  constructor(
    public navCtrl: NavController,
    private documentViewer: DocumentViewer,
    private file: File,
    private fileOpener: FileOpener,
    public alertController: AlertController,
    private filePath: FilePath,
    public platform: Platform,
    private accessProvider: AccessProvider,
    private callNumber: CallNumber,
    private emailComposer: EmailComposer,
    private iab: InAppBrowser
  ) {

  }

  /**
  * Opens PDF in native app
  */
  showBewerbung(){
    let path = this.file.applicationDirectory+'www/assets';
    let fileName = "Bewerbung_HenrikMeyer.pdf";
    if(!this.platform.is("ios")){
      path = this.file.applicationDirectory;
      this.filePath.resolveNativePath(path).then((native_path) => {
        let options: DocumentViewerOptions = {
          title: 'Bewerbung Henrik Meyer'
        };
        this.file.copyFile(native_path+"www/assets", fileName, this.file.externalDataDirectory, fileName).then(
          (copied) => {
            console.log("FILESYSTEM2: copied:" + JSON.stringify(copied));
            this.fileOpener.open(this.file.externalDataDirectory+fileName, 'application/pdf')
            .then(() => console.log("FILESYSTEM3: FILE OPENED"))
            .catch(e => console.log('FILESYSTEM3: Error opening file'+JSON.stringify(e)));
          }
        ).catch(
          (err) => {
            console.log("FILESYSTEM2: not copied "+JSON.stringify(err));
          }
        );
      });
    }
    else{
      let fileName = "Bewerbung_HenrikMeyer.pdf";
      let options: DocumentViewerOptions = {
        title: 'Bewerbung Henrik Meyer'
      };
      this.fileOpener.open(path+"/"+fileName, 'application/pdf')
      .then(() => console.log('File is opened'))
      .catch(e => console.log('Error opening file', e));
    }
  }

  /**
  * Opens PDF in native app
  */
  showArbeitszeugnis(){
    let path = this.file.applicationDirectory+'www/assets';
    let fileName = "Arbeitszeugnis.pdf";
    if(!this.platform.is("ios")){
      path = this.file.applicationDirectory;
      this.filePath.resolveNativePath(path).then((native_path) => {
        let options: DocumentViewerOptions = {
          title: 'Arbeitszeugnis'
        };
        this.file.copyFile(native_path+"www/assets", fileName, this.file.externalDataDirectory, fileName).then(
          (copied) => {
            console.log("FILESYSTEM2: copied:" + JSON.stringify(copied));
            this.fileOpener.open(this.file.externalDataDirectory+fileName, 'application/pdf')
            .then(() => console.log("FILESYSTEM3: FILE OPENED"))
            .catch(e => console.log('FILESYSTEM3: Error opening file'+JSON.stringify(e)));
          }
        ).catch(
          (err) => {
            console.log("FILESYSTEM2: not copied "+JSON.stringify(err));
          }
        );
      });
    }
    else{
      let fileName = "Arbeitszeugnis.pdf";
      let options: DocumentViewerOptions = {
        title: 'Arbeitszeugnis'
      };
      this.fileOpener.open(path+"/"+fileName, 'application/pdf')
      .then(() => console.log('File is opened'))
      .catch(e => console.log('Error opening file', e));
    }
  }

  /**
  * Opens PDF in native app
  */
  showBachelorurkunde(){
    let path = this.file.applicationDirectory+'www/assets';
    let fileName = "Bachelorurkunde.pdf";
    if(!this.platform.is("ios")){
      path = this.file.applicationDirectory;
      this.filePath.resolveNativePath(path).then((native_path) => {
        let options: DocumentViewerOptions = {
          title: 'Bachelorurkunde'
        };
        this.file.copyFile(native_path+"www/assets", fileName, this.file.externalDataDirectory, fileName).then(
          (copied) => {
            console.log("FILESYSTEM2: copied:" + JSON.stringify(copied));
            this.fileOpener.open(this.file.externalDataDirectory+fileName, 'application/pdf')
            .then(() => console.log("FILESYSTEM3: FILE OPENED"))
            .catch(e => console.log('FILESYSTEM3: Error opening file'+JSON.stringify(e)));
          }
        ).catch(
          (err) => {
            console.log("FILESYSTEM2: not copied "+JSON.stringify(err));
          }
        );
      });
    }
    else{
      let fileName = "Bachelorurkunde.pdf";
      let options: DocumentViewerOptions = {
        title: 'Bachelorurkunde'
      };
      this.fileOpener.open(path+"/"+fileName, 'application/pdf')
      .then(() => console.log('File is opened'))
      .catch(e => console.log('Error opening file', e));
    }
  }

  /**
  * Opens PDF in native app
  */
  showBetriebshelfer(){
    let path = this.file.applicationDirectory+'www/assets';
    let fileName = "Betriebshelfer.pdf";
    if(!this.platform.is("ios")){
      path = this.file.applicationDirectory;
      this.filePath.resolveNativePath(path).then((native_path) => {
        let options: DocumentViewerOptions = {
          title: 'Betriebshelfer-Bescheinigung'
        };
        this.file.copyFile(native_path+"www/assets", fileName, this.file.externalDataDirectory, fileName).then(
          (copied) => {
            console.log("FILESYSTEM2: copied:" + JSON.stringify(copied));
            this.fileOpener.open(this.file.externalDataDirectory+fileName, 'application/pdf')
            .then(() => console.log("FILESYSTEM3: FILE OPENED"))
            .catch(e => console.log('FILESYSTEM3: Error opening file'+JSON.stringify(e)));
          }
        ).catch(
          (err) => {
            console.log("FILESYSTEM2: not copied "+JSON.stringify(err));
          }
        );
      });
    }
    else{
      let fileName = "Betriebshelfer.pdf";
      let options: DocumentViewerOptions = {
        title: 'Betriebshelfer-Bescheinigung'
      };
      this.fileOpener.open(path+"/"+fileName, 'application/pdf')
      .then(() => console.log('File is opened'))
      .catch(e => console.log('Error opening file', e));
    }
  }

  /**
  * Opens AGBs in system browser
  */
  showDatenschutz(){
    let options = "";
    const browser = this.iab.create('https://www.henry-spaceman.com/datenschutz', '_system', options);

  }

  /**
  * Opens native phone app with number
  */
  call(){
    this.callNumber.callNumber("017684350106", true)
    .then(res => {
      console.log('Launched dialer!', res);
    })
    .catch(err => console.log('Error launching dialer', err));
  }


}
