import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the EditNotePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-note',
  templateUrl: 'edit-note.html',
})
export class EditNotePage {
  index:number = -1;
  notes:any;
  text:string = "";

  /**
  * Creates an instance of EditNotePage
  */
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage
  ) {
    this.index = this.navParams.get("noteIndex");
    this.storage.get("notes").then((data)=>{
      this.notes = JSON.parse(data);
      if(this.index>-1){
        this.text = this.notes[this.index];
      }
    });
  }

  /**
  * Saves note and returns to preview page
  */
  save(){
    if(this.index == -1){
      this.notes.push(this.text);
    }
    else{
      this.notes[this.index]=this.text;
    }
    this.storage.set("notes", JSON.stringify(this.notes));
    this.navCtrl.pop();
  }
}
