import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { EditNotePage } from '../edit-note/edit-note';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';

/**
 * Generated class for the NotesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-notes',
  templateUrl: 'notes.html',
})
export class NotesPage {

  notes:any = [];
  reorder:boolean = false;

  /**
  * Creates an instance of NotesPage
  */
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private storage: Storage,
    private socialSharing: SocialSharing
  ) {

  }

  /**
  * Opens EditNotePage by index
  */
  goToEditNote(index){
    this.navCtrl.push(EditNotePage, {noteIndex: index});
  }

  /**
  * Refreshes notes
  */
  ionViewDidEnter() {
    this.storage.get("notes").then((data)=>{
      if(data!=null){
        this.notes = JSON.parse(data);
      }
      else{
        this.storage.set("notes", JSON.stringify([]));
      }
    });
  }

  /**
  * Reorders notes
  */
  reorderItems(indexes) {
    let element = this.notes[indexes.from];
    this.notes.splice(indexes.from, 1);
    this.notes.splice(indexes.to, 0, element);
    this.save();
  }

  /**
  * Saves notes
  */
  save(){
    this.storage.set("notes", JSON.stringify(this.notes));
  }

  /**
  * Show and hide reorder button
  */
  toggleReorder(){
    this.reorder = !this.reorder;
  }

  /**
  * Shares a note by native share client
  */
  share(text){
    this.socialSharing.share(text);
  }

  /**
  * deletes a note
  */
  delete(noteIndex){
    this.notes.splice(noteIndex, 1);
    this.save();
  }
}
