import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AccessProvider } from '../../providers/access/access';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})
export class AboutPage {

  /**
  * Creates an instance of AboutPage
  */
  constructor(
    public navCtrl: NavController,
    private iab: InAppBrowser,
    public alertController: AlertController,
    private accessProvider: AccessProvider,
    private callNumber: CallNumber,
    private emailComposer: EmailComposer
  ) {

  }

  /**
  * Opens xing in system browser
  */
  goToXing(){
    let url = "https://www.xing.com/profile/Henrik_Meyer16";
    let options = "location=no,closebuttoncaption=Fertig,hardwareback=yes,toolbarposition=top,closebuttoncolor=#FFF100,zoom=yes,toolbarcolor=#707070,enableViewportScale=yes,presentationstyle=formsheet,hideurlbar=yes";
    const browser = this.iab.create(url, '_system', options);
  }

  /**
  * Opens linkedin in system browser
  */
  goToLinkedIn(){
    let url = "https://www.linkedin.com/in/henrik-meyer-456266109";
    let options = "location=no,closebuttoncaption=Fertig,hardwareback=yes,toolbarposition=top,closebuttoncolor=#FFF100,zoom=yes,toolbarcolor=#707070,enableViewportScale=yes,presentationstyle=formsheet,hideurlbar=yes";
    const browser = this.iab.create(url, '_system', options);
  }

  /**
  * Opens website in sytem browser
  */
  goToWebsite(){
    let url = "https://www.henry-spaceman.com";
    let options = "location=no,closebuttoncaption=Fertig,hardwareback=yes,toolbarposition=top,closebuttoncolor=#FFF100,zoom=yes,toolbarcolor=#707070,enableViewportScale=yes,presentationstyle=formsheet,hideurlbar=yes";
    const browser = this.iab.create(url, '_system', options);
  }

  /**
  * Opens the native Call-Client with number
  */
  call(){
    this.callNumber.callNumber("017684350106", true)
    .then(res => {
      console.log('Launched dialer!', res);
    })
    .catch(err => console.log('Error launching dialer', err));
  }

  /**
  * Opens the native Mail-Client with recipient and subject
  */
  mail(){
    let email = {
      to: 'henrik.mey@web.de',
      subject: 'Bewerbung Henrik Meyer',
      body: ''
    }
    this.emailComposer.open(email);
  }

}
