import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { ImprintPage } from '../imprint/imprint'
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  /**
  * Creates an instance of HomePage
  */
  constructor(
    public navCtrl: NavController,
    private emailComposer: EmailComposer,
    private callNumber: CallNumber,
    private iab: InAppBrowser,
  ) {

  }

  /**
  * Opens native phone app
  */
  call(){
    this.callNumber.callNumber("017684350106", true)
    .then(res => {
      console.log('Launched dialer!', res);
    })
    .catch(err => console.log('Error launching dialer', err));
  }

  /**
  * Opens native mail client
  */
  mail(){
    let email = {
      to: 'henrik.mey@web.de',
      subject: 'Bewerbung Henrik Meyer',
      body: ''
    }
    // Send a text message using default options
    this.emailComposer.open(email);
  }

  /**
  * Opens AGBs in system browser
  */
  goToImprint(){
    let url = "https://henry-spaceman.com/impressum";
    let options = "location=no,closebuttoncaption=Fertig,hardwareback=yes,toolbarposition=top,closebuttoncolor=#FFF100,zoom=yes,toolbarcolor=#707070,enableViewportScale=yes,presentationstyle=formsheet,hideurlbar=yes";
    const browser = this.iab.create(url, '_system', options);
  }

  /**
  * Opens AGBs in system browser
  */
  goToTerms(){
    let url = "https://henry-spaceman.com/datenschutz";
    let options = "location=no,closebuttoncaption=Fertig,hardwareback=yes,toolbarposition=top,closebuttoncolor=#FFF100,zoom=yes,toolbarcolor=#707070,enableViewportScale=yes,presentationstyle=formsheet,hideurlbar=yes";
    const browser = this.iab.create(url, '_system', options);
  }

}
