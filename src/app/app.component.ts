import { Component } from '@angular/core';
import { Platform, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

import { TabsPage } from '../pages/tabs/tabs';
import { StartPage } from '../pages/start/start';
import { App } from 'ionic-angular';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = StartPage;

  constructor(
    platform: Platform,
    statusBar: StatusBar,
    splashScreen: SplashScreen,
    private screenOrientation: ScreenOrientation,
    public alertController: AlertController,
    public app: App
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      if(platform.is("cordova")){
        if(platform.is("ios")){
          statusBar.styleDefault();
        }
        else{
          statusBar.styleLightContent();
        }

        splashScreen.hide();
        screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      }

      platform.registerBackButtonAction(() => {
        // Catches the active view
        let nav = this.app.getActiveNavs()[0];
        let activeView = nav.getActive();
        // Checks if can go back before show up the alert
        if(activeView.name === 'TabPage') {
          if (nav.canGoBack()){
            nav.pop();
          }
          else {
            const alert = this.alertController.create({
              title: 'App schließen',
              message: 'Wollen Sie die App schließen?',
              buttons: [{
                text: 'Nein',
                role: 'cancel',
              },{
                text: 'Ja',
                handler: () => {
                  platform.exitApp();
                }
              }]
            });
            alert.present();
          }
        }
      });
    });
  }
}
