import { NgModule } from '@angular/core';
import { AnimateItemsDirective } from './animate-items/animate-items';
@NgModule({
	declarations: [AnimateItemsDirective],
	imports: [],
	exports: [AnimateItemsDirective]
})
export class DirectivesModule {}
